package cn.xusc.facoty;

import cn.xusc.definition.BeanDefinition;
import cn.xusc.util.ProxyUtil;
import lombok.NonNull;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*********************************************
 *                抽象bean工厂                 *
 *********************************************/
public abstract class AbstractBeanFactory {

    // bean定义缓存
    private final Map<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(256);

    // 单列bean缓存 - refresh执行
    protected List<String> singleBeanNameList = new ArrayList<>(128);

    // 实际获取bean实现，由子类做具体实现
    public abstract Object doGetSingleBean(String beanName);

    // 初始化所有单列非懒加载bean
    public abstract void doSingleBeanInitialization();

    // 销毁失败的beans
    public abstract void destroySingleBeans();

    // 通过bean名获取bean对象
    public Object getBean(String beanName){
        BeanDefinition beanDefinition;
        if ((beanDefinition = beanDefinitionMap.get(beanName)) == null) {
            throw new IllegalStateException("beanName：[ " + beanName +" ] not register");
        }
        if (beanDefinition.isSingle()) {
            return doGetSingleBean(beanName);
        }
        return initBean(beanName);
    }

    // 通过bean类获取bean对象
    public Object getBean(@NonNull Class beanClass){
        return getBean(beanClass.getName());
    }

    // 通过类注册bean定义
    public void registerBeanDefinition(Class clazz) {
        this.registerBeanDefinition(clazz,false, true);
    }

    // 通过类、懒加载属性、单列属性注册bean定义
    public void registerBeanDefinition(@NonNull Class clazz, boolean lazyInitialized, boolean single) {
        BeanDefinition beanDefinition = new BeanDefinition(clazz, lazyInitialized, single);
        beanDefinitionMap.put(beanDefinition.getClassName(),beanDefinition);
        if (beanDefinition.isSingle() && !beanDefinition.isLazyInitialized()) {
            singleBeanNameList.add(beanDefinition.getClassName());
        }
    }

    // 初始化bean
    public Object initBean(String beanName){
        Object bean = null;
        BeanDefinition beanDefinition;
        if ((beanDefinition = beanDefinitionMap.get(beanName)) == null) {
            throw new IllegalStateException("beanName：[ " + beanName +" ] not register");
        }

        try {
            if (beanDefinition.isLazyInitialized()) {
                // lazy
                bean = ProxyUtil.proxy(beanDefinition);
            } else {
                // promptly
                bean = beanDefinition.getClazz().getDeclaredConstructor().newInstance();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return bean;
    }

    // 实例化所有剩余的单列（未懒加载的）
    public void singleBeanInitialization() {
        doSingleBeanInitialization();
    }

    // 清除通用缓存
    public void clearCommonCaches(){
        this.singleBeanNameList.clear();
        this.singleBeanNameList = null; // - help gc
    }

}
