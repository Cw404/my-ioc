package cn.xusc.facoty;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*********************************************
 *               bean工厂                     *
 *********************************************/
public class BeanFactory extends AbstractBeanFactory{
    /** Cache of singleton objects: bean name to bean instance. */
    private final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    @Override
    public void doSingleBeanInitialization() {
        this.singleBeanNameList.forEach(beanName -> {
             this.singletonObjects.put(beanName,initBean(beanName));

        });
    }

    @Override
    public void destroySingleBeans() {
        this.singletonObjects.clear();
    }

    // 最终获取bean对象的实现
    @Override
    public Object doGetSingleBean(String beanName) {
        Object bean;
        if ((bean = this.singletonObjects.get(beanName)) == null) {
            //init bean
            synchronized (singletonObjects) {
                // DCL
                if ((bean = this.singletonObjects.get(beanName)) == null) {
                    bean = initBean(beanName);
                    singletonObjects.put(beanName, bean);
                }
            }
        }
        return bean;
    }
}
