package cn.xusc.app;

import cn.xusc.facoty.AbstractBeanFactory;
import cn.xusc.facoty.BeanFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/*********************************************
 *                应用上下文                   *
 *********************************************/
@Slf4j
public class ApplicationContext {
    /** Synchronization monitor for the "refresh" and "destroy". */
    private final Object startupShutdownMonitor = new Object();

    private final AtomicBoolean active = new AtomicBoolean();

    private BeanFactory beanFactory;

    public ApplicationContext() {
        this.beanFactory = new BeanFactory();
    }

    public ApplicationContext(Class... classes) {
        this();
        // 循环注入bean定义
        Arrays.asList(classes).forEach(clazz -> registerBeanDefinition(clazz));
        refresh();
    }

    // 通过类注入bean定义
    public void registerBeanDefinition(Class clazz) {
        beanFactory.registerBeanDefinition(clazz);
    }

    // 通过类和懒加载属性注入bean定义
    public void registerBeanDefinition(Class clazz, boolean lazyInitialized) {
        beanFactory.registerBeanDefinition(clazz,lazyInitialized,true);
    }

    // 通过类、懒加载属性、单例属性注入bean定义
    public void registerBeanDefinition(Class clazz, boolean lazyInitialized, boolean single) {
        beanFactory.registerBeanDefinition(clazz,lazyInitialized,single);
    }

    // 刷新上下文
    public void refresh(){
        synchronized (this.startupShutdownMonitor) {
            // 1.准备刷新
            prepareRefresh();

            try {
                // 2.实例化所有剩余的单列（未懒加载的）
                finishBeanFactoryInitialization(beanFactory);

                // 3.完成刷新
                finishRefresh();
            }catch (Exception e) {
                // 销毁失败的beans
                destroyBeans();

                // 退出刷新
                cancelRefresh();
                throw e;
            } finally {
                // 4.重置通用缓存
                resetCommonCaches();
            }
        }
    }

    private void prepareRefresh() {
        this.active.set(true);
    }

    private void finishBeanFactoryInitialization(AbstractBeanFactory beanFactory) {
        beanFactory.singleBeanInitialization();
    }

    private void finishRefresh() {
        log.info("ApplicationContext init success");
    }

    private void destroyBeans() {
        this.beanFactory.destroySingleBeans();
    }

    private void resetCommonCaches() {
        beanFactory.clearCommonCaches();
    }

    private void cancelRefresh() {
        this.active.set(false);
    }

    // 是否刷新成功
    public boolean isActive() {
        return this.active.get();
    }

    // 通过类获取bean对象
    public Object getBean(Class beanClass) {
        return beanFactory.getBean(beanClass);
    }

    // 通过类名获取bean对象
    public Object getBean(String beanName) {
        return beanFactory.getBean(beanName);
    }
}
