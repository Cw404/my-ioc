package cn.xusc.definition;

import lombok.*;

/*********************************************
 *                 bean定义                   *
 *********************************************/
@Setter
@Getter
@NoArgsConstructor
public class BeanDefinition {
    // 类
    private Class clazz;

    // 类名
    private String className;

    // 懒加载状态
    private boolean lazyInitialized;

    // 单例状态(默认为单例)
    private boolean single;

    public BeanDefinition(Class clazz) {
        this(clazz,false,true);
    }

    public BeanDefinition(@NonNull Class clazz, boolean lazyInitialized, boolean single) {
        this.clazz = clazz;
        this.className = clazz.getName();
        this.lazyInitialized = lazyInitialized;
        this.single = single;
    }
}
