package cn.xusc.util;

import cn.xusc.definition.BeanDefinition;
import cn.xusc.model.Object;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.locks.ReentrantLock;

/*********************************************
 *         代理帮助类 - JDK动态代理实现          *
 *********************************************/
@Slf4j
public final class ProxyUtil {

    // 重入锁
    private static final ReentrantLock LOCK = new ReentrantLock();

    public static Object proxy (@NonNull BeanDefinition beanDefinition){
        return (Object) Proxy.newProxyInstance(
                ProxyUtil.class.getClassLoader(),
                new Class[]{Object.class},
                new InvocationHandler(){
                    private boolean init; // 初始化标识
                    private java.lang.Object object;// 懒加载bean对象

                    // 初始化对象
                    private void initObject (){
                        LOCK.lock();
                        try {
                            if (!init) {
                                log.info("init");
                                object = beanDefinition.getClazz().getDeclaredConstructor().newInstance();
                                init = true;
                            }
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } finally {
                            LOCK.unlock();
                        }
                    }

                    @Override
                    public java.lang.Object invoke(java.lang.Object proxy, Method method, java.lang.Object[] args) throws Throwable {
                        log.info("invoke");
                        if (!init) {
                            //执行初始化
                            initObject();
                        }
                        return method.invoke(object,args);
                    }
                }
        );
    }
}
