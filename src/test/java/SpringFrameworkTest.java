import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*********************************************
 *                SpringFramework测试         *
 *********************************************/
public class SpringFrameworkTest {
    private Logger log = LoggerFactory.getLogger(SpringFrameworkTest.class);

    @Test
    public void Test(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"spring.xml"},false);
        context.refresh();
        log.info("{}",context.getBean("systemPrint"));
        log.info("{}",context.getBean("systemPrint"));
    }
}
