import cn.xusc.app.ApplicationContext;
import cn.xusc.model.User;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainTest {
    static final Logger LOG = LoggerFactory.getLogger(MainTest.class);// 获取日志对象

    @Test
    public void ApplicationContextTest() {
        // 构建应用上下文
        ApplicationContext context = new ApplicationContext();
        // 注册一个懒加载的User bean
        context.registerBeanDefinition(User.class,false,true);
        // 刷新应用上下文
        context.refresh();
        // 获取IOC容器bean对象
        Object bean = context.getBean(User.class);
        // 触发懒加载
        LOG.info("{}",bean);
    }
}
